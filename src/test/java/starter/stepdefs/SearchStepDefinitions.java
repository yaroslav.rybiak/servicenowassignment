package starter.stepdefs;

import endpoints.GithubEndpoint;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertTrue;

public class SearchStepDefinitions {

    private static final String BASE_URI = GithubEndpoint.BASE.getUrl();

    private static int status;
    private static StringBuffer content = new StringBuffer();

    @When("User searches for Java")
    public void userSearchesForJava() throws Exception {
        URL url = new URL(BASE_URI + "language:Java");
        createConnection(url);
    }

    @When("User searches for username")
    public void userSearchesForUser() throws Exception {
        URL url = new URL(BASE_URI + "yaroslav-rybiak%20in:name");
        createConnection(url);
    }

    @When("User searches for Java or JavaScript")
    public void userSearchesForJavaAndJs() throws Exception {
        URL url = new URL(BASE_URI + "language:Java%20and%20language:JavaScript&sort=stars&order=desc");
        createConnection(url);
    }

    @When("User searches for top 10 repos from current year")
    public void userSearchesForTop2022Repos() throws Exception {
        URL url = new URL(BASE_URI + "created:>2022-01-01&sort=stars&order=desc&per_page=10");
        createConnection(url);
    }

    @Then("User receives status code {int}")
    public boolean userReceivesStatusCode(int statusCode) {
        return status == statusCode;
    }

    @Then("User can see total count")
    public void userCanSeeTotalCount() {
        assertNotEquals(content.indexOf("total_count"), -1);
    }

    @Then("User can see Java repositories")
    public void userCanSeeJavaRepos() {
        assertNotEquals(content.indexOf("Java"), -1);
    }

    @Then("User can see Java and JS repositories")
    public void userCanSeeJavaAndJsRepos() {
        assertTrue(
                content.indexOf("Java") != -1 &
                        content.indexOf("JavaScript") != -1);
    }

    @Then("User can see usernames repos")
    public void userCanSeeUSERNAME() {
        assertNotEquals(content.indexOf("yaroslav-rybiak"), -1);
    }

    private void createConnection(URL url) throws Exception {
        HttpURLConnection con = (HttpURLConnection) url.openConnection();
        con.setRequestMethod("GET");
        status = con.getResponseCode();
        BufferedReader in = new BufferedReader(
                new InputStreamReader(con.getInputStream()));
        String inputLine;
        while ((inputLine = in.readLine()) != null) {
            content.append(inputLine);
        }
        in.close();
    }
}
