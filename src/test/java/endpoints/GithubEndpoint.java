package endpoints;

public enum GithubEndpoint {
    BASE("https://api.github.com/search/repositories?q=");
    private final String url;

    GithubEndpoint(String url) {
        this.url = url;
    }

    public String getUrl() {
        return url;
    }
}
