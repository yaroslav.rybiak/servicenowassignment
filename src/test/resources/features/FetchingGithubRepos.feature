Feature: Fetching Github repositories

  Scenario: Get total number of GitHub repositories for a Java language
    When User searches for Java
    Then User receives status code 200
    And User can see Java repositories

  Scenario: Get total number of GitHub repositories for Java+JavaScript languages
    When User searches for Java or JavaScript
    Then User receives status code 200
    And User can see Java and JS repositories

  Scenario: Get total number of GitHub repositories by username
    When User searches for username
    Then User receives status code 200
    And User can see usernames repos

  Scenario: Get top starred repositories from 2022
    When User searches for top 10 repos from current year
    Then User receives status code 200
    And User can see total count