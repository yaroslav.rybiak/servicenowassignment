# What was done:

1. Created framework based on Serenity to use Cucumber/Gherkin scenarios
2. Created 4 scenarios to cover 6 cases: src/test/resources/features/
3Automated steps for each scenario without using RestAssured or any API framework
   (as per requirements): src/test/resources/stepdefs/
3. Added html report generation (after running tests open target/site/serenity/index.html)
4. Created pipeline to run tests on remote CI/CD server (gitlab)

# Run instructions:
## Locally:
Use **mvn clean verify** to run tests locally (given local machine has java and maven installed)
This will run tests and generate html report in target/site/serenity/index.html

## Gitlab:
If you are a contributor: go to https://gitlab.com/yaroslav.rybiak/servicenowassignment/-/pipelines
and click **Run pipeline**. HTML report is available under **Artifacts -> Download artifacts**. After downloading,
open target/site/serenity/index.html in browser. If you are not a contributor, you still can see previous runs
and download artifacts or fork a project and run fork by yourself.
